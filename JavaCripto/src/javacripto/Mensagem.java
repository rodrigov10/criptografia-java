package javacripto;

import java.io.IOException;
import java.io.PrintStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Rodrigo Vianna
 * @since 19/05/2019
 */
public class Mensagem {

    public static void enviarMensagem(String msg, PublicKey publicKey, PrintStream printStream) throws IOException, Exception {
        byte[] bytesCifrados = Criptografia.criptografar(msg.getBytes(), publicKey, "RSA");
        String textoCifrado = Arrays.toString(bytesCifrados);
        System.out.println("Texto cifrado:" + textoCifrado);
        printStream.println(textoCifrado);
        printStream.flush();
    }

    public static String receberMensagem(Scanner scanner, PrivateKey privateKey) throws Exception {
        String[] msgCript = scanner.nextLine().replace("[", "").replace("]", "").replace("|", "").split(", ");
        byte[] bytesRecebidos = new byte[msgCript.length];
        for (int i = 0; i < msgCript.length; i++) {
            bytesRecebidos[i] = Byte.parseByte(msgCript[i]);
        }
        byte[] bytesDecifrados = Criptografia.decrypt(bytesRecebidos, privateKey, "RSA");
        return new String(bytesDecifrados);
    }
}
