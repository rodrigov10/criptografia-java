package javacripto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Scanner;
import javax.crypto.Cipher;

/**
 *
 * @author Rodrigo Vianna
 * @since 17/05/2019
 */
public class Criptografia {

    public static byte[] criptografar(byte[] inpBytes, PublicKey key, String xform) throws Exception {
        Cipher cipher = Cipher.getInstance(xform);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(inpBytes);
    }

    public static byte[] decrypt(byte[] inpBytes, PrivateKey key, String xform) throws Exception {
        Cipher cipher = Cipher.getInstance(xform);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(inpBytes);
    }

    public static void enviarChave(OutputStream output, PublicKey publicKey) throws IOException {
        PrintStream printStream = new PrintStream(output);
        byte[] bytePublicKey = publicKey.getEncoded();
        System.out.println(Arrays.toString(bytePublicKey));
        printStream.println(Arrays.toString(bytePublicKey));
        printStream.flush();
    }

    public static PublicKey receberChave(InputStream input) throws IOException, ClassNotFoundException, NoSuchAlgorithmException, InvalidKeySpecException {
        Scanner scanner = new Scanner(input);
        String[] chaveString = scanner.nextLine().replace("[", "").replace("]", "").replace("|", "").split(", ");
        byte[] content = new byte[chaveString.length];
        System.out.println(Arrays.toString(chaveString));
        for (int i = 0; i < chaveString.length; i++) {
            content[i] = Byte.parseByte(chaveString[i]);
        }
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(content));
    }
}
