package javacripto;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rodrigo
 */
public class JCliente2 extends javax.swing.JFrame {

    public static final int PORTA = 12345;

    private ServerSocket serverSocket;
    private Socket socket;
    private OutputStream ou;

    private PrintStream printStreamOutro;

    private PrivateKey chavePrivada;
    private PublicKey chavePublica;

    public JCliente2() {
        initComponents();
    }

    private void enviarMensagem(String msg) throws IOException, Exception {
        Mensagem.enviarMensagem(msg, chavePublica, printStreamOutro);
        jTextArea.append(msg + "\r\n");
        jTextField.setText("");
    }

    private void conectar() throws IOException, ClassNotFoundException {
        try {
            serverSocket = new ServerSocket(PORTA);
            socket = new Socket("127.0.0.1", JCliente1.PORTA);
            Socket socketOutro = serverSocket.accept();
            ou = socket.getOutputStream();

            KeyPairGenerator geradorChaveRSA = KeyPairGenerator.getInstance("RSA");
            geradorChaveRSA.initialize(512);
            KeyPair chaves = geradorChaveRSA.generateKeyPair();

            PublicKey chavePublica = chaves.getPublic();
            chavePrivada = chaves.getPrivate();

            this.chavePublica = Criptografia.receberChave(socket.getInputStream());
            System.out.println("Chave pública recebida!");

            OutputStream ouOutro = socketOutro.getOutputStream();
            Criptografia.enviarChave(ouOutro, chavePublica);

            printStreamOutro = new PrintStream(ouOutro);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void escutar() throws IOException, Exception {
        Scanner scanner = new Scanner(socket.getInputStream());
        while (true) {
            if (scanner.hasNext()) {
                String msg = Mensagem.receberMensagem(scanner, chavePrivada);
                System.out.println("Texto decifrado:" + msg);
                jTextArea.append(msg + "\r\n");
            }
        }
    }

    private void sair() throws Exception {
        try {
            enviarMensagem("Sair");
            printStreamOutro.close();
            ou.close();
            socket.close();
        } catch (IOException e) {
            Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public static void main(String[] args) {
        try {
            JCliente2 app = new JCliente2();
            app.setVisible(true);
            app.conectar();
            new Thread(() -> {
                try {
                    app.escutar();
                } catch (IOException ex) {
                    Logger.getLogger(JCliente1.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane = new javax.swing.JScrollPane();
        jTextArea = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel = new javax.swing.JLabel();
        jTextField = new javax.swing.JTextField();
        jButton_Enviar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jButton_Sair = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Cliente 2");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jTextArea.setEditable(false);
        jTextArea.setBackground(new java.awt.Color(240, 240, 240));
        jTextArea.setColumns(20);
        jTextArea.setRows(5);
        jScrollPane.setViewportView(jTextArea);

        getContentPane().add(jScrollPane, java.awt.BorderLayout.CENTER);

        jPanel1.setMaximumSize(new java.awt.Dimension(1000, 52));
        jPanel1.setMinimumSize(new java.awt.Dimension(200, 52));
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 52));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));

        jPanel3.setMaximumSize(new java.awt.Dimension(1000, 25));
        jPanel3.setMinimumSize(new java.awt.Dimension(100, 25));
        jPanel3.setPreferredSize(new java.awt.Dimension(400, 25));
        jPanel3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 1));

        jLabel.setText("Mensagem");
        jPanel3.add(jLabel);

        jTextField.setMaximumSize(new java.awt.Dimension(200, 22));
        jTextField.setMinimumSize(new java.awt.Dimension(200, 22));
        jTextField.setPreferredSize(new java.awt.Dimension(200, 22));
        jTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextFieldKeyPressed(evt);
            }
        });
        jPanel3.add(jTextField);

        jButton_Enviar.setText("Enviar");
        jButton_Enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_EnviarActionPerformed(evt);
            }
        });
        jPanel3.add(jButton_Enviar);

        jPanel1.add(jPanel3);

        jPanel4.setMaximumSize(new java.awt.Dimension(1000, 25));
        jPanel4.setMinimumSize(new java.awt.Dimension(100, 25));
        jPanel4.setPreferredSize(new java.awt.Dimension(400, 25));
        jPanel4.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 1));

        jButton_Sair.setText("Sair");
        jButton_Sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_SairActionPerformed(evt);
            }
        });
        jPanel4.add(jButton_Sair);

        jPanel1.add(jPanel4);

        getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(416, 409));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_EnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_EnviarActionPerformed
        try {
            enviarMensagem(jTextField.getText());
        } catch (IOException e) {
            Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, e);
        } catch (Exception ex) {
            Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton_EnviarActionPerformed

    private void jButton_SairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_SairActionPerformed
        try {
            sair();
        } catch (Exception ex) {
            Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton_SairActionPerformed

    private void jTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                enviarMensagem(jTextField.getText());
            } catch (IOException e) {
                Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, e);
            } catch (Exception ex) {
                Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jTextFieldKeyPressed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            sair();
        } catch (Exception ex) {
            Logger.getLogger(JCliente2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_Enviar;
    private javax.swing.JButton jButton_Sair;
    private javax.swing.JLabel jLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JTextArea jTextArea;
    private javax.swing.JTextField jTextField;
    // End of variables declaration//GEN-END:variables
}
